package open.games.rpgengine.domain.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 * @author alexa
 */
@Entity
public class Area extends DescribedConcept {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "room_id")
	private Room room;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(inverseJoinColumns = @JoinColumn(name = "item_id"))
	private Set<Item> items;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(inverseJoinColumns = @JoinColumn(name = "npc_id"))
	private Set<Npc> npcs;

	public Room getRoom() {
		return room;
	}

	public void setRoom(final Room room) {
		this.room = room;
	}

	public Set<Npc> getNpcs() {
		return npcs;
	}

	public void setNpcs(final Set<Npc> npcs) {
		this.npcs = npcs;
	}

	public Set<Item> getItems() {
		if (items == null) {
			items = new HashSet<>();
		}
		return items;
	}

	public void setItems(final Set<Item> items) {
		this.items = items;
	}
}
