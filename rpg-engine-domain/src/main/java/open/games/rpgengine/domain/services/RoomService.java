package open.games.rpgengine.domain.services;

import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Room;

import java.util.List;

/**
 * Created by alexa on 15-Aug-19.
 */
public interface RoomService {

    /**
     * @return the rooms.
     */
    List<Room> getRooms();

	List<Area> getAreasForRoom(Long id);

	void addRoom(String name, String code);

	void addAreaInRoom(Long roomId, String name, String code);
}
