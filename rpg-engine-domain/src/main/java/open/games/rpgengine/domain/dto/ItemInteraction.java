package open.games.rpgengine.domain.dto;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;

@Entity
public class ItemInteraction {

	private static final String AVAILABILITY_AREA = "IN_AREA";
	private static final String AVAILABILITY_HAND = "IN_HAND";
	private static final String AVAILABILITY_CONTAINER = "IN_CONTAINER";

	private String interactionName;

	@ElementCollection
	private List<String> availability;
}
