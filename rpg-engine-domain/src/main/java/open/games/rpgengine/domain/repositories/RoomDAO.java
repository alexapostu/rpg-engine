package open.games.rpgengine.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import open.games.rpgengine.domain.dto.Room;

/**
 * @author alexa
 */
public interface RoomDAO extends JpaRepository<Room, Long> {
}
