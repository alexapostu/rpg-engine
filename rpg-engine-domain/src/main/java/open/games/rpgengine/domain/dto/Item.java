package open.games.rpgengine.domain.dto;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

/**
 * @author alexa
 */
@Entity
public class Item extends DescribedConcept {
	private boolean canBePicked;

	private List<ItemInteraction> interactions;

	public boolean isCanBePicked() {
		return canBePicked;
	}

	public void setCanBePicked(final boolean canBePicked) {
		this.canBePicked = canBePicked;
	}

	public List<ItemInteraction> getInteractions() {
		if (interactions == null) {
			interactions = new ArrayList<>();
		}
		return interactions;
	}

	public void setInteractions(final List<ItemInteraction> interactions) {
		this.interactions = interactions;
	}
}
