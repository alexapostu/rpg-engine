package open.games.rpgengine.domain.dto;

import javax.persistence.Entity;

/**
 * @author alexa
 * @date 16-Aug-19.
 */
@Entity
public class Npc extends DescribedConcept {

    private boolean allowsInteration;

    private boolean isAttackable;

    public boolean isAllowsInteration() {
        return allowsInteration;
    }

    public void setAllowsInteration(final boolean allowsInteration) {
        this.allowsInteration = allowsInteration;
    }

    public boolean isAttackable() {
        return isAttackable;
    }

    public void setAttackable(final boolean attackable) {
        isAttackable = attackable;
    }
}
