package open.games.rpgengine.domain.services;

import java.util.List;

import open.games.rpgengine.domain.dto.Item;

public interface ItemService {
	void addItem(Item item);

	List<Item> getItems();

	void deleteItem(Long id);
}
