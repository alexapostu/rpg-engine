package open.games.rpgengine.domain.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Item;

/**
 * @author alexa
 */
public interface AreaDAO extends JpaRepository<Area, Long> {

	@Query("SELECT a FROM Area a WHERE ?1 MEMBER OF a.items")
	List<Area> getAreasContainingItem(Item item);
}
