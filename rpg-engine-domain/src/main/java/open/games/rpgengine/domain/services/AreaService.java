package open.games.rpgengine.domain.services;

public interface AreaService {
	void addItemToArea(Long itemId, Long areaId);
}
