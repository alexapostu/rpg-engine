package open.games.rpgengine.domain.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * @author alexa
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Concept {

	/** This concept's id. */
	@Id
	@GeneratedValue
	private Long id;

	/** This concept's code. */
	@Column(unique = true, nullable = false)
	private String code;

	/**
	 * @return the id.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set.
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set.
	 */
	public void setCode(final String code) {
		this.code = code;
	}
}
