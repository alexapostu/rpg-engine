package open.games.rpgengine.domain.dto;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * @author alexa
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class DescribedConcept extends Concept {

	/** The room's name. */
	private String name;

	/** The room's description */
	private String description;

	/**
	 * @return the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set.
	 */
	public void setDescription(final String description) {
		this.description = description;
	}
}
