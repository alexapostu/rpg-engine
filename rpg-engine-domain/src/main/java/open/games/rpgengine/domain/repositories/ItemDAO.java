package open.games.rpgengine.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import open.games.rpgengine.domain.dto.Item;

public interface ItemDAO extends JpaRepository<Item, Long> {
}
