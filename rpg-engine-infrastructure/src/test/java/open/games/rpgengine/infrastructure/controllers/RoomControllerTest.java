package open.games.rpgengine.infrastructure.controllers;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import open.games.rpgengine.infrastructure.builders.dto.RoomBuilder;
import open.games.rpgengine.domain.dto.Room;
import open.games.rpgengine.domain.services.RoomService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author alexa
 */
@RunWith(MockitoJUnitRunner.class)
public class RoomControllerTest {
	@Mock
	public RoomService roomService;
	@InjectMocks
	private RoomController sut;

	@Test
	public void getRooms() {
		List<Room> returnedList = Collections.singletonList(new RoomBuilder().get());
		when(sut.getRooms()).thenReturn(returnedList);

		assertEquals(returnedList, sut.getRooms());
		verify(roomService, times(1)).getRooms();
	}

}
