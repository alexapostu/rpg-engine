package open.games.rpgengine.infrastructure.services;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import open.games.rpgengine.infrastructure.builders.dto.RoomBuilder;
import open.games.rpgengine.domain.dto.Room;
import open.games.rpgengine.domain.repositories.RoomDAO;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * @author alexa
 */
@RunWith(MockitoJUnitRunner.class)
public class RoomServiceImplTest {

	@Mock
	private RoomDAO roomDAO;

	@InjectMocks
	private RoomServiceImpl sut;

	@Test
	public void getRooms() {
		List<Room> returnedList = Collections.singletonList(new RoomBuilder().get());
		when(sut.getRooms()).thenReturn(returnedList);

		assertEquals(returnedList, sut.getRooms());
		verify(roomDAO, times(1)).findAll();
	}
}
