package open.games.rpgengine.infrastructure.builders.dto;

import open.games.rpgengine.domain.dto.Room;

public class RoomBuilder extends DescribedConceptBuilder<RoomBuilder, Room> {
	public RoomBuilder() {
		super(new Room());
	}
}
