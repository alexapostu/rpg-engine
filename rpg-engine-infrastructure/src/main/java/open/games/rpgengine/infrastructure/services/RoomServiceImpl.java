package open.games.rpgengine.infrastructure.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import open.games.rpgengine.infrastructure.builders.dto.AreaBuilder;
import open.games.rpgengine.infrastructure.builders.dto.RoomBuilder;
import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Room;
import open.games.rpgengine.domain.repositories.AreaDAO;
import open.games.rpgengine.domain.repositories.RoomDAO;
import open.games.rpgengine.domain.services.RoomService;

/**
 * The service responsible for working with rooms.
 * @author alexa
 */
@Component
public class RoomServiceImpl implements RoomService {

	/** The dao used when persisting and retrieving rooms. */
	private final RoomDAO roomDAO;
	/** The dao used when persisting and retrieving areas. */
	private final AreaDAO areaDAO;

	/**
	 * @param roomDAO the dao used when persisting and retrieving rooms.
	 * @param areaDAO
	 * @param areaBuilder
	 */
	public RoomServiceImpl(final RoomDAO roomDAO, final AreaDAO areaDAO) {
		this.roomDAO = roomDAO;
		this.areaDAO = areaDAO;
	}

	@Override
	public List<Room> getRooms() {
		return roomDAO.findAll();
	}

	@Override
	public List<Area> getAreasForRoom(final Long roomId) {
		final Area areaByRoomExample = new AreaBuilder().withRoom(new RoomBuilder().withId(roomId).get()).get();
		return areaDAO.findAll(Example.of(areaByRoomExample));
	}

	@Override
	public void addRoom(final String name, final String code) {
		Room r = new RoomBuilder().withCode(code).withName(name).get();
		roomDAO.save(r);
	}

	@Override
	public void addAreaInRoom(final Long roomId, final String name, final String code) {
		final Optional<Room> room = roomDAO.findById(roomId);
		if (!room.isPresent()) {
			throw new IllegalArgumentException("There is no room having id: " + roomId);
		}
		Area area = new AreaBuilder().withCode(code).withName(name).withRoom(room.get()).get();
		areaDAO.save(area);
	}
}
