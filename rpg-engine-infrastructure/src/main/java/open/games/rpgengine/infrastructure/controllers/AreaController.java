package open.games.rpgengine.infrastructure.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import open.games.rpgengine.domain.services.AreaService;

@RestController
public class AreaController {
	private AreaService areaService;

	private AreaController(final AreaService areaService) {
		this.areaService = areaService;
	}

	@PutMapping("/area/{areaId}/item")
	public void addItemToArea(@PathVariable final Long areaId, @RequestParam("itemId") final Long itemId) {
		areaService.addItemToArea(itemId, areaId);
	}
}
