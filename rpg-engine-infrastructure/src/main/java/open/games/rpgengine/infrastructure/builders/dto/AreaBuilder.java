package open.games.rpgengine.infrastructure.builders.dto;

import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Item;
import open.games.rpgengine.domain.dto.Room;

public class AreaBuilder extends DescribedConceptBuilder<AreaBuilder, Area> {

	public AreaBuilder() {
		super(new Area());
	}

	public AreaBuilder withRoom(final Room room) {
		get().setRoom(room);
		return self();
	}

	public AreaBuilder withItem(final Item item) {
		get().getItems().add(item);
		return self();
	}
}
