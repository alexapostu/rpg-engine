package open.games.rpgengine.infrastructure.builders.dto;

import open.games.rpgengine.domain.dto.DescribedConcept;

public class DescribedConceptBuilder<B extends DescribedConceptBuilder<B, T>, T extends DescribedConcept> extends ConceptBuilder<B, T> {
	public DescribedConceptBuilder(final T concept) {
		super(concept);
	}

	public B withName(final String name) {
		get().setName(name);
		return self();
	}

	public B withDescription(final String description) {
		get().setDescription(description);
		return self();
	}
}
