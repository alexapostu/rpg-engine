package open.games.rpgengine.infrastructure.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Room;
import open.games.rpgengine.domain.services.RoomService;

/**
 * Controller in charge of Room operations.
 * @author alexa
 */
@RestController
public class RoomController {

	/**
	 * Service used when working with rooms.
	 */
	private final RoomService roomService;

	/**
	 * @param roomService the service used when working with rooms
	 */
	public RoomController(final RoomService roomService) {
		this.roomService = roomService;
	}

	@GetMapping("/rooms/{id}/areas")
	public List<Area> getAreasForRoom(@PathVariable final Long id) {
		return roomService.getAreasForRoom(id);
	}

	/**
	 * @return the rooms.
	 */
	@GetMapping("/rooms")
	public List<Room> getRooms() {
		return roomService.getRooms();
	}

	@PostMapping("/rooms")
	public void addRoom(@RequestParam final String name, @RequestParam final String code) {
		roomService.addRoom(name, code);
	}

	@PostMapping("/rooms/{id}/areas")
	public void addAreaInRoom(@PathVariable final Long id, @RequestParam final String name, @RequestParam final String code) {
		roomService.addAreaInRoom(id, name, code);
	}
}
