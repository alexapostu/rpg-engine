package open.games.rpgengine.infrastructure.builders.dto;

import open.games.rpgengine.domain.dto.Concept;

public class ConceptBuilder<B extends ConceptBuilder<B, T>, T extends Concept> {
	private final T concept;

	public ConceptBuilder(final T concept) {
		this.concept = concept;
	}

	@SuppressWarnings("unchecked")
	protected B self() {
		return (B) this;
	}

	public B withId(final Long id) {
		concept.setId(id);
		return self();
	}

	public B withCode(final String code) {
		concept.setCode(code);
		return self();
	}

	public T get() {
		return concept;
	}
}
