package open.games.rpgengine.infrastructure.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import open.games.rpgengine.domain.dto.Item;
import open.games.rpgengine.domain.services.ItemService;

@RestController
public class ItemController {

	private ItemService itemService;

	private ItemController(final ItemService itemService) {
		this.itemService = itemService;
	}

	@PostMapping("/items")
	public void addItem(final Item item) {
		itemService.addItem(item);
	}

	@GetMapping("/items")
	public List<Item> getItems() {
		return itemService.getItems();
	}

	@DeleteMapping("/items/{id}")
	public void deleteItem(@PathVariable final Long id) {
		itemService.deleteItem(id);
	}
}
