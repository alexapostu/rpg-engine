package open.games.rpgengine.infrastructure.services;

import java.util.List;

import org.springframework.stereotype.Component;

import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Item;
import open.games.rpgengine.domain.repositories.AreaDAO;
import open.games.rpgengine.domain.repositories.ItemDAO;
import open.games.rpgengine.domain.services.ItemService;

@Component
public class ItemServiceImpl implements ItemService {

	private ItemDAO itemDAO;
	private AreaDAO areaDAO;

	private ItemServiceImpl(final ItemDAO itemDAO, final AreaDAO areaDAO) {
		this.itemDAO = itemDAO;
		this.areaDAO = areaDAO;
	}

	@Override
	public void addItem(final Item item) {
		itemDAO.save(item);
	}

	@Override
	public List<Item> getItems() {
		return itemDAO.findAll();
	}

	@Override
	public void deleteItem(final Long itemId) {
		// First remove the item from all the areas containing it.
		Item item = itemDAO.getOne(itemId);
		List<Area> areasContainingItem = areaDAO.getAreasContainingItem(item);
		for (Area area : areasContainingItem) {
			area.getItems().remove(item);
		}
		areaDAO.saveAll(areasContainingItem);
		itemDAO.deleteById(itemId);
	}
}
