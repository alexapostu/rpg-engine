package open.games.rpgengine.infrastructure.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import open.games.rpgengine.domain.dto.Area;
import open.games.rpgengine.domain.dto.Item;
import open.games.rpgengine.domain.repositories.AreaDAO;
import open.games.rpgengine.domain.repositories.ItemDAO;
import open.games.rpgengine.domain.services.AreaService;

@Service
public class AreaServiceImpl implements AreaService {

	private AreaDAO areaDAO;
	private ItemDAO itemDAO;

	private AreaServiceImpl(final AreaDAO areaDAO, final ItemDAO itemDAO) {
		this.areaDAO = areaDAO;
		this.itemDAO = itemDAO;
	}

	@Override
	public void addItemToArea(final Long itemId, final Long areaId) {
		Optional<Item> itemToSave = itemDAO.findById(itemId);
		Optional<Area> areaToSave = areaDAO.findById(areaId);
		if (!itemToSave.isPresent() || !areaToSave.isPresent()) {
			throw new IllegalArgumentException("Missing item or area. Item id: " + itemId + ". Area id: " + areaId);
		}
		final Area area = areaToSave.get();
		area.getItems().add(itemToSave.get());
		areaDAO.save(area);
	}
}
