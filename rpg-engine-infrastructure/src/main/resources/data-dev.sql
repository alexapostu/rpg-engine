INSERT INTO room(id, code, description, name) VALUES
  (1, 'room_1', 'The first room', 'Room 1'),
  (2, 'room_2', 'The second room', 'Room 2');

INSERT INTO area(id, code, description, room_id) VALUES (101, 'room_1', 'This is the first area in the room', 1);
